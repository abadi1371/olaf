function loadObj(fileName,textureName,color,cb)
{

    var manager = new THREE.LoadingManager();
    var loader = new THREE.FBXLoader(manager);

    loader.load(fileName, function (object) {
        object.traverse(function (child) {
            if (child instanceof THREE.Mesh) {
                if(textureName)
                child.material.map =  THREE.ImageUtils.loadTexture(textureName);

                else
                {
                   // child.material.ambient.setHex(color);
					child.material = new THREE.MeshPhongMaterial({
  color: color,   
  
						reflectivity:0 ,shininess:0
						
						
});
child.material.neadsUpdate=true;
                   // child.material.color.setHex(color);
					//child.material.opacity = 0.5;
					//child.material.transparent = true
                }

            }
            cb(object);

        });
 })

}


function addParticle(particleCount ,textureName , color , size , scatteringDistance,){
   var loader = new THREE.TextureLoader();
   var texture;
   loader.load(textureName,function(image){texture = image;})

   // var particles = new THREE.Geometry();
//    var pMaterial = new THREE.ParticleBasicMaterial({
//     color:color ,size:size,map: THREE.ImageUtils.TextureLoader(),
//blending
//     transparent: true
// });
    var pMaterial = new THREE.MeshLambertMaterial({
        map: THREE.ImageUtils.loadTexture(textureName),
        transparent: true
    });
  
   let particleGeo = new THREE.PlaneBufferGeometry(size,size);
   let particles = [];
    for (var i = 0; i < particleCount; i++) {
        let particle = new THREE.Mesh(particleGeo,pMaterial);
        particle.position.set(Math.random() * scatteringDistance.x.range + scatteringDistance.x.min,
        Math.random() * scatteringDistance.y.range + scatteringDistance.y.min,
        Math.random() * scatteringDistance.z.range + scatteringDistance.z.min
        );
       
        particle.material.opacity=0.55;
        particles.push(particle);
        
        // var pX = Math.random() * scatteringDistance.x.range + scatteringDistance.x.min,
        //     pY = Math.random() * scatteringDistance.y.range + scatteringDistance.y.min,
        //     pZ = Math.random() * scatteringDistance.z.range + scatteringDistance.z.min,
        //     particle = new THREE.Vector3(pX, pY, pZ);
           
        // particle.velocity = new THREE.Vector3(0, -Math.random(), 0);
        // particles.vertices.push(particle);
    }

    // var particleSystem = new THREE.ParticleSystem(particles, pMaterial);
    // particleSystem.sortParticles = true;
    // particleSystem.geometry.verticesNeedUpdate = true;

    return particles

}


function loadPlane(){
     var loader = new THREE.TextureLoader();
   
      var geometry = new THREE.PlaneGeometry( 200, 200 ,32);
    //  var material = new THREE.MeshLambertMaterial({
    //     map: THREE.ImageUtils.loadTexture("./images/cloud5.png"),
    //     transparent: true
    // });
    

var supGif = new SuperGif({ gif: document.getElementById('gif') } );
supGif.load();
var gifCanvas = supGif.get_canvas();
var material = new THREE.MeshStandardMaterial();
material.map = new THREE.Texture( gifCanvas );
material.displacementMap = material.map;
var plane = new THREE.Mesh( geometry, material );
 plane.position.y = -100;

  return {plane,material}
}


function loadParticles2(particleCount ,textureName1,textureName2 , color , size , scatteringDistance){
     

    particles = new THREE.Group();
    const geo = new THREE.PlaneGeometry(size,size);
    const mat1 = new THREE.MeshLambertMaterial({
        map: THREE.ImageUtils.loadTexture(textureName1),
        transparent: true
    });
	const mat2 = new THREE.MeshLambertMaterial({
        map: THREE.ImageUtils.loadTexture(textureName2),
        transparent: true
    });
	
    var particle;
	var firstXPosition = Math.random() *scatteringDistance.x.range + scatteringDistance.x.min;
	var firstYPosition = Math.random() *scatteringDistance.y.range + scatteringDistance.y.min;
    for(let i=0; i<particleCount; i++) {
         particle = i%2==0 ? new THREE.Mesh(geo,mat1) : new THREE.Mesh(geo,mat2) ;
        // particle.velocity = new THREE.Vector3(
		// 	Math.random() ,
        //     scatteringDistance.y.min, 
        //    Math.random());
       // particle.acceleration = new THREE.Vector3(0,-0.001,0);
        particle.position.x = i==0 ? firstXPosition : firstXPosition+(i*(size+50));
        particle.position.z = Math.random() *scatteringDistance.z.range + scatteringDistance.z.min;
        particle.position.y = i==0 ? firstYPosition : firstYPosition+(i*(size-100));
        particles.add(particle)
    }
    
    return particles


}

function loadParticles3(particleCount  , color , size , scatteringDistance){
     
      var particles = [];
    var pMaterial =  new THREE.MeshBasicMaterial({
        map: THREE.ImageUtils.loadTexture("./images/glass5.png"),
        transparent: true,
       });
       pMaterial.opacity = 0.8;
       var group = new THREE.Group();
 var geometry = new THREE.BoxGeometry( size,size,size);
 //var material = new THREE.MeshBasicMaterial( {color: color} );
 
        for (var i = 0; i < particleCount; i++) {
            var particle = new THREE.Mesh( geometry, pMaterial );
              particle.position.set(Math.random() * scatteringDistance.x.range + scatteringDistance.x.min,
              Math.random() * scatteringDistance.y.range + scatteringDistance.y.min,
              Math.random() * scatteringDistance.z.range + scatteringDistance.z.min); 

             
             
         group.add(particle);
     }
 
    
     return group
}