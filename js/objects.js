function objectModels(){
    return [
        {parentName:"models/new2/parent/11rotate (2).fbx",
        childName:"models/new2/child/5.fbx",
       yPosition:50,delay:130,parentObj:null,childObj:null},
        {parentName:"models/new2/parent/12.FBX",
        childName:"models/new2/child/6.fbx",
        yPosition:-80,delay:130,parentObj:null,childObj:null},
        {parentName:"models/new2/parent/1.FBX",
        childName:"models/new2/child/1.fbx",
        yPosition:-210,delay:130,parentObj:null,childObj:null},
        {parentName:"models/new2/parent/10.FBX",
        childName:"models/new2/child/8.fbx",
        yPosition:-340,delay:130,parentObj:null,childObj:null},
        {parentName:"models/new2/parent/5.FBX",
        childName:"models/new2/child/4.fbx",
        yPosition:-470,delay:130,parentObj:null,childObj:null},
        {parentName:"models/new2/parent/8.FBX",
        childName:"models/new2/child/12.fbx",
        yPosition:-600,delay:130,parentObj:null,childObj:null},
		{parentName:"models/new2/parent/17.FBX",
        childName:"models/new2/child/13.fbx",
        yPosition:-730,delay:130,parentObj:null,childObj:null},
        {parentName:"models/new2/parent/3.FBX",
        childName:"models/new2/child/2.fbx",
        yPosition:-860,delay:130,parentObj:null,childObj:null},
        {parentName:"models/new2/parent/9.FBX",
        childName:"models/new2/child/3.fbx",
        yPosition:-990,delay:130,parentObj:null,childObj:null},
        {parentName:"models/new2/parent/4.FBX",
        childName:"models/new2/child/7.fbx",
        yPosition:-1120,delay:130,parentObj:null,childObj:null},
        {parentName:"models/new2/parent/19.FBX",
        childName:"models/new2/child/9.fbx",
        yPosition:-1250,delay:130,parentObj:null,childObj:null},
		{parentName:"models/new2/parent/16.FBX",
        childName:"models/new2/child/18.fbx",
        yPosition:-1380,delay:130,parentObj:null,childObj:null},
        {parentName:"models/new2/parent/14.FBX",
        childName:"models/new2/child/17.fbx",
        yPosition:-1510,delay:130,parentObj:null,childObj:null},
		{parentName:"models/new2/parent/15.FBX",
        childName:"models/new2/child/15.fbx",
        yPosition:-1640,delay:130,parentObj:null,childObj:null},
    
    ]
}

function colors(index){
    switch(index)
    {
            case 0:
            return {parentColor:0x3459eb,childColor:0xa2b3f6}
            case 1:
            return {parentColor:0x4700b3,childColor:0x3459eb}
            case 2:
            return {parentColor:0x6600cc,childColor:0x4700b3}
            case 3:
            return {parentColor:0x006633,childColor:0x6600cc}
            case 4:
            return {parentColor:0x007a99,childColor:0x006633}
            case 5:
            return {parentColor:0x600080,childColor:0xd966ff}
            case 6:
            return {parentColor:0x8f246b,childColor:0x600080}
            case 7:
            return {parentColor:0x004d39,childColor:0x8f246b}
            case 8:
            return {parentColor:0x4400cc,childColor:0x004d39}
            case 9:
            return {parentColor:0x0077b3,childColor:0x4400cc}
            case 10:
            return {parentColor:0x4d0019,childColor:0x0077b3}
			case 11:
            return {parentColor:0x4d194d,childColor:0x4d0019}
			case 12:
            return {parentColor:0x4c0080,childColor:0x4d194d}
			case 13:
            return {parentColor:0x131339,childColor:0x4c0080}
    }

}

function scales(index){
    switch(index)
    {
             case 0:
            return {childScale:30,parentScale:35,parentHeight:30,childHeight:30}
            case 1:
            return {childScale:44,parentScale:50,parentHeight:30,childHeight:30}
            case 2:
            return {childScale:54,parentScale:70,parentHeight:30,childHeight:30}
            case 3:
            return {childScale:64,parentScale:75,parentHeight:30,childHeight:30}
            case 4:
            return {childScale:70,parentScale:75,parentHeight:30,childHeight:30}
            case 5:
            return {childScale:70,parentScale:70,parentHeight:30,childHeight:30}
            case 6:
            return {childScale:75,parentScale:70,parentHeight:30,childHeight:30}
            case 7:
            return {childScale:78,parentScale:80,parentHeight:30,childHeight:30}
            case 8:
            return {childScale:80,parentScale:80,parentHeight:30,childHeight:30}
            case 9:
            return {childScale:80,parentScale:80,parentHeight:30,childHeight:30}
            case 10:
            return {childScale:80,parentScale:80,parentHeight:30,childHeight:30}
			case 11:
            return {childScale:80,parentScale:80,parentHeight:30,childHeight:30}
			case 12:
            return {childScale:80,parentScale:80,parentHeight:30,childHeight:30}
			case 13:
            return {childScale:80,parentScale:80,parentHeight:30,childHeight:30}
    }
}

function telorances(index){
    switch(index)
    {
            case 0:
            return 15
            case 1:
            return 15
            case 2:
            return 15
            case 3: case 7:
            return 15
		case 4: case 5: case 6:   case 8:  case 9: case 10: case 11: case 12:case 13:
            return 15
            
    }
}

function symmetryAngles(index)
{
   /* switch(index)
    {
            case 0: case 1:  case 2:
            return Math.PI/3
            case 3: case 7:
            return Math.PI/3
			case 6:
			return Math.PI/4
		case 4: case 5:     case 8:  case 9: case 10: case 11: case 12: case 13:
            return Math.PI/6
            
    } */
	
	return Math.PI/3
}

function cameraDistanceDelta(index)
{
    switch(index)
    {
            case 0: case 1:  case 2:
            return -0.7
            case 3: 
            return -10
			case 4: case 5:
			return -10
            case 6: case 7: case 8:  
            return 10
            case 9: case 10: 
            return 15
               
            
    } 
}